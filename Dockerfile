FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src
COPY */Poli.API.csproj .
RUN dotnet restore
COPY . .
RUN dotnet publish Poli.ServicioAPI.sln -c release -o /app

FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build app .
EXPOSE 5000/tcp
ENTRYPOINT ["dotnet", "Poli.API.dll"]
