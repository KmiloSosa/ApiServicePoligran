using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Poli.Infrastructure;
using Poli.Core;

namespace Poli.Core.Tests
{
    public class WeatherServiceTest
    {
        private IWeatherService _service = default!;
        private ISummariesRepository _repo = default!;

        public WeatherServiceTest()
        {         
            _repo = new SummariesRepository();
            _service = new WeatherService(_repo);
        }

        [Fact]
        public void VeriryIfThereAreFiveElements()
        {
            //arrange
            IEnumerable<WeatherForecast> summaries = _service.GetAll();
            //act
            var quatityElements = summaries.ToList();
            //assert

            Assert.Equal(5,quatityElements.Count);
        }
    }
}