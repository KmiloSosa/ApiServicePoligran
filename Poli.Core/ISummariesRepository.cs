using System;

namespace Poli.Core
{
    public interface ISummariesRepository
    {
        string[] GetSummaries();
    }
}