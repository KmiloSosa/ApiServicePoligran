using System;
using System.Collections.Generic;

namespace Poli.Core
{
   public interface IWeatherService
    {
        IEnumerable<WeatherForecast> GetAll();
    }
}