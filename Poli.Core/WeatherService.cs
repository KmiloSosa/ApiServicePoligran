using System;
using System.Collections.Generic;
using System.Linq;

namespace Poli.Core
{
    public class WeatherService : IWeatherService
    {
        private readonly ISummariesRepository _repo;
        public WeatherService(ISummariesRepository repo)
        {
            _repo = repo;
        }
        public IEnumerable<WeatherForecast> GetAll()
        {
            var rng = new Random();
            var Summaries = GetSummaries();

            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        private string[] GetSummaries(){
            return _repo.GetSummaries();
        }
    }
}