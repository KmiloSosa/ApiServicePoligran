using System;
using Poli.Core;

namespace Poli.Infrastructure
{
   public class SummariesRepository : ISummariesRepository
    {
        public SummariesRepository()
        {
            
        }
        private string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public string[] GetSummaries()
        {
            return Summaries;
        }
    }
}